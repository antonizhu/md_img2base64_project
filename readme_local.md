# readme

## 一、项目简介

### 问题来源

每次本地写好markdown格式的文档后，想要将它上传到博客网站上，但是本地的图片无法直接复制到网站的博客页面，每个图片需要重新点击上传，然后上传本地文件。其实也可以买一个图床解决，奈何囊中羞涩，并且还是不想将图片保存到别人的服务器。于是，想通过 markdown文档插入base64图片的方法，完成方便地进行博文上传。

### 目标功能、问题及解决方案

因此，想要实现功能：

* 将已有的markdown文本转化为插入了base64图片编码的markdown

要实现上述功能，存在几个问题：

* base64编码太长了，不利于博文的观看
* base64编码太长了，导致文本内容极大

以下面两种方法来解决：

* 针对base64编码太长了，不利于博文观看的问题。
  * 解决方法：将base64的编码都放到博文的末尾，这样就可以不影响博文编写模式下的观看。[reference:base64的高级方法](https://www.jianshu.com/p/280c6a6f2594)

* base64编码太长了，导致文本内容极大
  * 解决方法: 对图像大小进行压缩, 其相应的base64编码也会压缩(当然,也会牺牲图片的清晰度)

综上所述,**我要实现的是**:

**通过一个python程序将 本地的markdown文件及文件关联的本地图片, 生成一个新的只有 base64 编码的markdown文件(观看效果与原文件相同,且完全不依赖原文件).**

## 二、源程序思路简介

### **思路**:

通过正则表达式搜索源文件中的图片格式，然后替换成base64的格式，base64编码在文件最后追加，并保存为新文件。

### 代码片段

引入依赖包

```python
from PIL import Image
import os
import base64
import re
```

函数: 图片转base64

```python
#将地址为address的图片转为base64字符串
def phtot_base64(address):
    with open(address,"rb") as photo:
        pb=base64.b64encode(photo.read())
        return str(pb)[2:-1]
```

函数：获得文件的大小(单位kb)

```python
# 获取文件大小:KB
def get_size(file):
    size = os.path.getsize(file)
    return size / 1024
```

函数：压缩图像

```python
def compress_image(infile_path, outfile_path, aim_size, step=10, quality=80):
    """不改变图片尺寸压缩到指定大小
    :param infile: 压缩源文件
    :param outfile: 压缩文件保存地址
    :param aim_size: 压缩目标，KB
    :param step: 每次调整的压缩比率
    :param quality: 初始压缩比率
    :return: 压缩文件地址，压缩文件大小
    """
    o_size = get_size(infile_path)
    # 如果当前图片大小小于压缩目标大小
    if o_size <= aim_size:
        im = Image.open(infile_path)
        im = im.convert('RGB')  # 为了存成jpg文件，将图像转成rgb
        im.save(outfile_path, quality=quality)  # 保存压缩后的图像
        print("图片已被压缩!")
        return infile_path
    
	# 如果当前图片大小大于压缩目标大小
    while o_size > aim_size:
        im = Image.open(infile_path)
        im = im.convert('RGB')  # 为了存成jpg文件，将图像转成rgb
        im.save(outfile_path, quality=quality)  # 保存压缩后的图像
        if quality - step < 0:
            break
        quality -= step
        o_size = get_size(outfile_path)

    print("图片已被压缩!")
    return outfile_path
```

## 三、效果展示及使用说明

文件目录如下：

![image-20210422213420443](pic/image-20210422213420443.png)

我们现在对【test.md】进行格式转化。pic文件夹中有图片如下，且已经被【test.md】引用：

![image-20210422213554616](pic/image-20210422213554616.png)

**执行【img_to_base64.py】!!!**，需要输入原始markdown路径，及希望压缩图片到多大。运行及打印结果如下：

```
(base) PS E:\mydoc\done\2021\summer\md_img2base64_project\tests> python .\img_to_base64.py

******************************
请输入目标markdown文件地址:
"E:\mydoc\done\2021\summer\md_img2base64_project\tests\test.md"
******************************

******************************
请输入压缩图片允许最大体积(单位：KB): 100
******************************

******************************
图片:【pic/image-20210422132634431.png】被读取!

原始图像base64文本长度为:28816
图片已被压缩!
压缩图像base64文本长度为:28816
******************************

******************************
图片:【pic/image-20210422132812199.png】被读取!

原始图像base64文本长度为:233024
图片已被压缩!
压缩图像base64文本长度为:21488
******************************


******************************
文件图片转换成功！
******************************
请按任意键继续. . .
```

之后文件目录编程这样：

![image-20210422214111891](pic/image-20210422214111891.png)



【对比两个markdown文本】：

原文件：

```markdown
![image-20210422132634431](pic/image-20210422132634431.png)

阿斯顿发撒打发



### 方法士大夫

/撒大大



$\sigma$

![image-20210422132812199](pic/image-20210422132812199.png)
```

转换后文件：

```markdown
![image-20210422132634431][Fig1]

阿斯顿发撒打发



### 方法士大夫

/撒大大



$\sigma$

![image-20210422132812199][Fig2]

****************************************************************************************************
  
****************************************************************************************************
  
****************************************************************************************************

[Fig1]:data:image/png;base64,iVBORw0KGgoAAAANSUhE...(省略)

[Fig2]:data:image/png;base64,iVBORw0KGgoAAAANSUhE...(省略)
```



完美解决，从此告别图床服务器！

**我还将该程序打包为可执行的exe文件，只要将文件放到markdown文件所在目录即可。**



## 文件夹介绍

|   文件夹名    |            文件夹介绍             | 备注 |
| :-----------: | :-------------------------------: | :--: |
|     build     |    pyinstaller生成的build文件     |      |
|     dist      | pyinstaller生成的distribution文件 |      |
| md_img2base64 |     virtualenv生成的虚拟环境      |      |
|      src      |              源代码               |      |
|     tests     |         测试时使用的文件          |      |
|      pic      |         readme文件的图片          |      |



